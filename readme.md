# gcloud 세팅
- 계정이 여러개여서 스위칭 해야할때 다음 명령어를 사용한다.
  - list: 설정값들 불러오기
  - activate: 해당 설정으로 계정 스위칭
  - create: 새 계정 생성 (이 때 gcloud auth login 으로 로그인 해줘야함)
```
gcloud config configurations
```

# 다른 회사 배포 과정
- 회사에 준비사항 알림(회사명, 회사 로고 이미지, 전화번호, 카카오톡 혹은 네이버 톡톡 주소, 문의 받을 이메일 주소, 사업자등록증, 통신판매신고번호 , 메타태그         (사이트설명, 카카오플러스친구, og이미지, 디스크립션, 파비콘, 키워드(약 15개) , 상단 고정메뉴 )) 
- 다른 회사의 추가 요청사항시 비용을 받고 진행(회사 별도의 사은품 처리(메인페이지 특별 사은품, 각 회사별 메인의 사은품 안내, 각각의 사은품 안내 팝업 이미지))
- gcp에 새 프로젝트 등록
- gcloud app deploy 로 app engine 을 시작하고 터미널 선택창에서 asia-northeast1 (supports standard and flexible) 선택
- bitbucket에 새로운 프로젝트에 맞는 새 bucket을 추가하기
- bitbucket에서 clone을 통해 새 폴더 만들기
- 만들어진 새 폴더에 기존 js-rental-main 소스코드 복사하기
- gcloud config set project 프로젝트명 으로 프로젝트 변경
- 필요한 부분(전화번호 등 수정)
- 도메인 구매
- gcp 콘솔에서 app engine > 설정 > 맞춤 도메인 이동 (app engine 시작 페이지가 나올시 14라인 참고할것))
- 맞춤 도메인 추가를 하여서 해당 도메인 소유주 인증(보통 txt방식 사용), 이 때 도메인을 구입한 곳에서 txt설정을 함
 {cafe24의 도메인의 유무 확인 - DNS관리 메뉴로 이동 = 도메인선택 후 DNS관리 클릭 = 
  GCP로 이동 후 좌측 햄버거메뉴에서 App Engine 의 설정으로 이동 = 맞춤도메인 탭으로 이동 = 맞춤도메인 추가 클릭
  이후 TXT 와 A레코드 코드를  획득 후 cafe24 A레코드 관리 TXT관리를 통해 추가(27번 라인) 이후 26번라인 진행후 확인}
- 인증이 완료되면 subdomain을 모두 추가(sk.domain.com, lg.domain.com, coway.domain.com ...)
- 도메인을 구입한 곳에서 A 설정(ip 변경) (A 레코드 추가)
- js-rental-main 에서 소스코드 복사
- info.js 에서 그 회사에 맞는 옵션으로 변경
- this.domain = 'rentalmoashop.com'; 의 부분을 배포될 페이지 도메인 주소로 변경
- gcloud config set project 를 통해 새 프로젝트 이름으로 변경
- 배포