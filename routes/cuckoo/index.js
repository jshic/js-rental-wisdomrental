const express = require('express');
const router = express.Router();

const asyncHandler = require('express-async-handler');

const renderModule = require('../../lib/renderModule');
const Info = require('../../info');

let info = new Info.cuckoo();

/* GET home page. */
// 메인 페이지
router.get('/', asyncHandler(async (req, res, next) => {
  let bannerList = await renderModule.getBanner(info);
  res.render(info.companyId+'/index', { info: info, bannerList: bannerList });
}));

// 제품 리스트 가져오기
router.get('/list', (req, res, next) => {
  res.redirect('/list/all');
});
// 제품 리스트 가져오기
router.get('/list/:category', asyncHandler(async (req, res, next) => {
  let goodsList = await renderModule.getGoodList(info, req.params);
  let cate2List = await renderModule.getCategory(info, req.params);
  res.render('list', { info: info, goodsList: goodsList, cate2: cate2List.list, cateParent: req.params.category, curCate: 'none' });
}));
// 제품 리스트 가져오기 / 하위 카테고리
router.get('/list/:category/:category2', asyncHandler(async (req, res, next) => {
  let goodsList = await renderModule.getGoodList(info, req.params);
  let cate2List = await renderModule.getCategory(info, req.params);
  res.render('list', { info: info, goodsList: goodsList, cate2: cate2List.list, cateParent: req.params.category, curCate: req.params.category2 });
}));

// 제품 상세 정보 가져오기
router.get('/detail/:gid', asyncHandler(async(req, res, next) => {
  let goodsDetail = await renderModule.getDetail(info, req.params);
  res.render('detail', { info: info, goodsDetail: goodsDetail });
}));

// 이벤트 가져오기
router.get('/event', asyncHandler(async(req, res, next) => {
  let eventList = await renderModule.getEvent(info);
  console.log(eventList);
  res.render('event', { info: info, eventList: eventList });
}));

module.exports = router;
