var express = require('express');
var router = express.Router();

const Info = require('../../info');

let info = new Info.main();

/* GET home page. */
router.get('/', function(req, res, next) {
  // 렌탈리스트의 open 여부를 가져와서 이미지 URL을 제공한다.
  let openList = [];
  for (let [key, value] of Object.entries(info.open)) {
    if (value) openList.push(key);
  }

  // 렌탈 이미지 및 주소 정보
  let rentalImgInfo = {
    sk: {
      url: '1_sk_logo.png',
      alt: 'SK렌탈',
      link: '//sk.'+info.domain,
    },
    lg: {
      url: '14_lg_logo.png',
      alt: 'LG 퓨리케어',
      link: '//lg.'+info.domain,
    },
    coway: {
      url: '2_cw_logo.png',
      alt: '웅진코웨이',
      link: '//coway.'+info.domain,
    },
    nice: {
      url: '3_ch_logo.png',
      alt: '청호나이스',
      link: '//nice.'+info.domain,
    },
    cuckoo: {
      url: '4_ck_logo.png',
      alt: '쿠쿠',
      link: '//cuckoo.'+info.domain,
    },
    hycare: {
      url: '5_hrc_logo.png',
      alt: '현대렌탈케어',
      link: '//hycare.'+info.domain,
    },
    welrix: {
      url: '6_wr_logo.png',
      alt: '웰릭스',
      link: '//welrix.'+info.domain,
    },
    ruhens: {
      url: '7_rhs_logo.png',
      alt: '루헨스',
      link: '//ruhens.'+info.domain,
    },
    hello: {
      url: '9_cj_hello_logo.png',
      alt: 'cj 헬로',
      link: '//hello.'+info.domain,
    },
    bs: {
      url: '11_bsr_logo.png',
      alt: 'bs 렌탈',
      link: '//bs.'+info.domain,
    },
    wells: {
      url: '8_wells_logo.png',
      alt: '교원웰스',
      link: '//wells.'+info.domain,
    },
    cs: {
      url: '13_csr_logo.png',
      alt: 'CS렌탈',
      link: '//cs.'+info.domain,
    },
    real: {
      url: '12_rr_logo.png',
      alt: 'REAL렌탈',
      link: '//real.'+info.domain,
    },
    smart: {
      url: '10_smr_logo.png',
      alt: 'SMART렌탈',
      link: '//smart.'+info.domain,
    },
    // biz: {
    //   url: 'interpark.png',
    //   alt: '인터파크 비즈마켓',
    //   link: '//biz.'+info.domain,
    // },    
    // hyrental: {
    //   url: 'hy_rental.png',
    //   alt: '현대Hy렌탈',
    //   link: '//hyrental.'+info.domain,
    // },
    // baro: {
    //   url: 'baro.png',
    //   alt: '바로 렌탈',
    //   link: '//baro.'+info.domain,
    // },    
  }
  
  let rentalStr = '';
  for(let i=0; i < 15; i++) {
    if (i < openList.length) {
      rentalStr += `
      <div class="col-md mb-3 mx-auto">
        <div class="card" style="border-radius: 10px;">
          <div class="card-body text-center zoom p-1">
            <a href="${rentalImgInfo[openList[i]].link}">
              <img src="/assets/img/logo/${rentalImgInfo[openList[i]].url}" alt="${rentalImgInfo[openList[i]].alt}" style="width:100%;">
            </a>              
          </div>
        </div>
      </div>
      `;
    }
    if ( (i+1) % 5 == 0 && (i+1) != 15) {
      rentalStr += `</div><div class="row">
      `;
    }
  }
  // console.log(rentalStr);
  res.render(info.companyId+'/index', { info: info, rentalStr: rentalStr });
});


/* 파트너사 모집페이지 */
router.get('/partner', function(req, res, next) {
  res.render('main/partner',{});
});


// 관리자 페이지로 들어오는 경우 js-rental 프로젝트의 관리자 페이지로 넘긴다
router.get('/admin', function(req, res, next) {
  res.redirect('http://js-rental.appspot.com/admin');
  return;
});

module.exports = router;
