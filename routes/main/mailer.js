const express = require("express");
const nodemailer = require("nodemailer");
const request = require("request");
const router = express.Router();
const Info = require("../../info");

let info = new Info.main();

router.post("/", function (req, res, next) {

  let tokenOptions = {};
  tokenOptions.url = "https://www.google.com/recaptcha/api/siteverify";
  tokenOptions.form = {
    secret: "6LdU_JkUAAAAALWaCb7KYOPrkFLuIER756MYgKb3",
    response: req.body.token
  };

  request.post(tokenOptions, (error, response, body) => {
    if (!error && response.statusCode == 200) {
      // console.log("mailer");
      let parseBody = JSON.parse(body);
      // console.log(parseBody);
      // console.log(parseBody.score);

      // 첫번째 글자가 8로 시작하면 차단한다.
      if (req.body.phone.charAt(0) == 8) {
        console.log("광고 전화로 의심돼서 차단함");
        res.end();
        return;
      }

      // 로봇 검증
      if (parseBody.score <= 0.4) {
        console.log("로봇으로 의심되서 차단함");
        res.end();
        return;
      } else {
        // 정상인 경우 메일 발송
        let poolConfig = {
          pool: true,
          host: "smtp.gmail.com",
          port: 465,
          secure: true, // use TLS
          auth: {
            user: info.mailerID,
            pass: info.mailerPW
          }
        };

        // request로 api서버에 기록을 남긴다.
        let logOptions = {
          uri: info.apiDomain+'/api/mailLog',
          method: 'POST',
          form : {
            name: (req.body.name)?req.body.name:'',
            coopName: info.coopName,
            productInfo: (req.body.modalProductInfo)?req.body.modalProductInfo:'', // 모델 이름 혹은 모델명
            phone: (req.body.phone)?req.body.phone:'',
            callTime: (req.body.callTime)?req.body.callTime:'',
            message: (req.body.message)?req.body.message:''
          }
        }
        // console.log('logOptions', logOptions);
        request.post(logOptions, (err) => {console.log('log sent', err)});
        
        // setup email data with unicode symbols
        let mailOptions = {
          from: `"${req.body.name}" <${req.body.email}>`, // sender address
          to: req.body.companyMail, // list of receivers
          subject: `[${req.body.companyId}] 렌탈문의 : ${req.body.name}님의 문의사항 입니다.`, // Subject line
          text: `
          ${req.body.companyName}에서 온 [${req.body.name}]님의 문의사항입니다.

          [제품명 혹은 모델명]:
          ${req.body.modalProductInfo}

          [성함]: 
          ${req.body.name}

          [연락처]: 
          ${req.body.phone}

          [연락 가능한 시간대]: 
          ${req.body.callTime}

          [문의 내용]: 
          ${req.body.message}
          ` // plain text body
        };

        let transporter = nodemailer.createTransport(poolConfig);

        transporter.sendMail(mailOptions, (error, info) => {
          if (error) {
            return console.log(error);
          }
          console.log("Message sent: %s", info.messageId);
        });

        // res.redirect('/');
        let backUrl = req.header('Referer') || '/';
        res.redirect(backUrl);
      }
    } else {
      console.log("getList error");
      console.log(error);
      // reject();
    }
  });


});

module.exports = router;
