const debug = require('debug')('js-rental-main:server');
const vhost = require('vhost');
const express = require('express');
const Info = require('./info'); // 최초 세팅은 이 파일에서 한다

const http = require('http');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const hbs = require('hbs');
const createError = require('http-errors');

const app = express();

let info = new Info.main();

// const app_main = require('./app_main');
// const app_sk = express();
// const app_coway = express();

const appList = {
  main: express(),
  sk: express(),
  coway: express(),
  lg: express(),
  cuckoo: express(),
  ruhens: express(),
  welrix: express(),
  hyrental: express(),
  hycare: express(),
  hello: express(),
  bs: express(),
  baro: express(),
  nice: express(),
  biz: express(),
  wells: express(),
  cs: express(),
  real: express(),
  smart: express(),
  partner: express(),
}

// 리스트 통합
for (let item in appList) {
  if (item != 'main' && !info.open[item]) continue;
      
  appList[item].set('views', path.join(__dirname, 'views'));
  appList[item].set('view engine', 'hbs');

  appList[item].use(logger('dev'));
  appList[item].use(express.json());
  appList[item].use(express.urlencoded({ extended: false }));
  appList[item].use(cookieParser());
  appList[item].use(express.static(path.join(__dirname, 'public')));

  appList[item].use('/', require('./routes/'+item+'/index'));
  if (item == 'main') appList[item].use('/mailer', require('./routes/main/mailer')); // main 앱은 메일 발송기가 들어있다.
  if (item == 'main') appList[item].use('/mailer_partner', require('./routes/main/mailer_partner')); // main 앱은 메일 발송기가 들어있다.


  // catch 404 and forward to error handler
  appList[item].use((req, res, next) => {
    next(createError(404));
  });

  // error handler
  appList[item].use((err, req, res, next) => {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error', {err:err});
  });

  if (item != 'main') {
    app.use(vhost(item+'.localhost', appList[item]));
    app.use(vhost(info.getDomain(item), appList[item]));
  }
  if (item == 'main') {
    // console.log('main vhost');
    // 메인은 개발용으로 추가 세팅
    app.use(vhost('localhost', appList[item]));
    app.use(vhost('jshic.iptime.org', appList[item]));
    app.use(vhost(info.getDomain(), appList[item]));
    app.use(vhost(info.getDomain('www'), appList[item]));
  }
}
// 공통 파셜 설정
hbs.registerPartials(__dirname + '/views/partials');

// 헬퍼
hbs.registerHelper('encodeURI', (url) => {
  return new hbs.SafeString(encodeURI(url));
});

hbs.registerHelper('ifCond', function(v1, v2, options) {
  if(v1 === v2) {
    return options.fn(this);
  }
  return options.inverse(this);
});

// app.use(express.static('static'));
app.use(express.static(__dirname + '/public'));

app.use((req, res) => {
  res.status(404).send('페이지를 찾을 수 없습니다. 주소가 올바른지 혹은 도메인 세팅이 되어 있는지 확인하여 주세요.');
});

/**
 * Get port from environment and store in Express.
 */

let port = normalizePort(process.env.PORT || '8080');
app.set('port', port);

let server = http.createServer(app);

// app.listen(8080, () => {
//   console.log('http://localhost:8080');
// });

server.listen(port);
console.log("http://localhost:"+port);
server.on('error', onError);
server.on('listening', onListening);

/**
 * Normalize a port into a number, string, or false.
 */

function normalizePort(val) {
  var port = parseInt(val, 10);

  if (isNaN(port)) {
    // named pipe
    return val;
  }

  if (port >= 0) {
    // port number
    return port;
  }

  return false;
}

/**
 * Event listener for HTTP server "error" event.
 */

function onError(error) {
  if (error.syscall !== 'listen') {
    throw error;
  }

  var bind = typeof port === 'string'
    ? 'Pipe ' + port
    : 'Port ' + port;

  // handle specific listen errors with friendly messages
  switch (error.code) {
    case 'EACCES':
      console.error(bind + ' requires elevated privileges');
      process.exit(1);
      break;
    case 'EADDRINUSE':
      console.error(bind + ' is already in use');
      process.exit(1);
      break;
    default:
      throw error;
  }
}

/**
 * Event listener for HTTP server "listening" event.
 */

function onListening() {
  var addr = server.address();
  var bind = typeof addr === 'string'
    ? 'pipe ' + addr
    : 'port ' + addr.port;
  debug('Listening on ' + bind);
}

module.exports = app;