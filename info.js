require('dotenv').config();

// 기본 ===================================================
class main {

  constructor() {
    this.coopName = '현명한렌탈'; // 이 서비스를 사용하는 회사명
    this.coopLogoFileName = 'hm_logo.png'; // 메인 회사 로고
    this.coopLogoFileNameFooter = 'hm_logo_2.png'; // 푸터용 로고
    this.companyId = 'main'; // 변경금지
    if (process.env.NODE_ENV != "dev") {
      // 실 서비스 URL
      this.domain = 'xn--sm2b78bs07a5fe7xa.com';
      this.apiDomain = 'https://js-rental.appspot.com';
    } else {
      // local 개발 URL
      this.domain = 'localhost:8080';
      // this.apiDomain = 'http://localhost:3004';
      this.apiDomain = 'https://js-rental.appspot.com';
    }     
    this.mailer = this.domain+'/mailer'; // 메일러 URL
    this.companyMail = 'summoner1@hanmail.net'; // 메일 문의 받는 주소
    this.mailerID = 'jshic8000@gmail.com'; // 메일 발송기 구글 ID // 보안 설정이 필요하다
    this.mailerPW = 'jshic2019^^'; // 메일 발송기 구글 패스워드
    this.db = {
      categoryId : '', // 데이터베이스 쿼리시 category1 로 가져올 키 이름
    }
    this.meta = {
      title: '현명한렌탈',
      description: '세상의 모든 렌탈 현명한렌탈',      
      canonical: 'https://www.xn--sm2b78bs07a5fe7xa.com',
      ogImage: '/assets/img/ogimg_hr.png', //sns 링크남길시에 나오는 이미지
      ogUrl: 'https://www.xn--sm2b78bs07a5fe7xa.com',
      faviconName: 'favicon.png',
      keyword: '렌탈, 렌탈샵, 렌탈문의',
    }
    this.config = {
      phone: '18339385', // 하이픈 추가하면 안됨 //tel용
      kakao: 'https://pf.kakao.com/_xoyLxbT',
      toctoc: '',
      mainColor: '#1974d6', // 이 사이트의 메인 컬러 // 컬러 코드 사용
      secondColor: '#000000', // 이 사이트의 보조 컬러
      thirdColor: '', // 이 사이트의 보조 컬러 2
      mainTextColor: 'indigo-text', // 이 사이트의 텍스트 컬러 컨셉 // 메인 컨셉과 유사한 색상 // 컬러 정보 사이트 -> https://mdbootstrap.com/docs/jquery/css/colors/
      subTextColor: 'white-text', // 이 사이트의 보조 텍스트 컬러 컨셉 // 메인에 대칭되는 색상 // 보통은 흰색
      navType: 'navbar-dark', // 네비게이션 밝기 - dark or light (dark라면 글씨가 하얀색으로 나옴)
      showLogo: true, // 상단에 로고를 보이지 않게 할때는 false로 한다.
      eventImg: 'event_button.png', // 모바일에서 좌측 하단 이벤트 아이콘 이미지
    }
    // 이 아래 순서로 메인 순서가 결정된다.
    this.open = {
      sk: true,
      coway: true,
      nice: true,
      cuckoo: true,
      hycare: true,
      welrix: true,
      ruhens: true,
      wells: true,
      hello: true,
      smart: true,
      bs: true,
      real: true,
      cs: true,
      lg: true,
      // biz: false,
      // baro: false,
      // hyrental: false,      
    },
    this.gnb = {
      on: false, // gnb가 false면 안나옴
      menu: [
        '정수기',
        '제빙기',
        '전기레인지',
        '의류건조기',
        '비데',
        '가스레인지',
        '안마의자',
        '식기세척기',
        '공기청정기',
      ],
      logoFileName: '',
    },
    this.companyInfo = {
      name:'(주)진성히크',// 법인명
      ceo:'김진홍',//대표님 성함
      bizNumber:'286-81-01086',//사업자 등록번호
      marketNumber:'제 2018-서울구로-0235호',//통신판매업 신고번호
      phone:'1833-9385',// 시각용 전화번호
      email:'k1236914@nate.com',//시각용 이메일
      address:'서울시 구로구 구로동 212-13번지 벽산디지털밸리 3차 11층 1104호',// 주소
      fax:'',//시각용 팩스번호

    }



  }

  getDomain(subdomain = null) {
    if (subdomain) {
      return subdomain+'.'+this.domain;
    } else {
      return this.domain;
    }
  }

} // class main


// SK 매직 ===================================================
class sk extends main {
  constructor() {
    super();
    this.companyId = 'sk'; // 내부적으로 사용되는 키
    this.meta.title = 'SK 매직';
    this.meta.description = '안녕하세요 SK 매직입니다.';
    this.config.mainColor= '#cc0033';
    this.config.secondColor= '#8C0023'; // 그라데이션에 사용
    this.config.mainTextColor= 'red-text';
    this.db.categoryId = 'SK'; // 관리자 페이지에 등록한 카테고리
    this.gnb = {
      on: true,
      menu: [
        '정수기',
        '제빙기',
        '전기레인지',
        '의류건조기',
        '비데',
        '가스레인지',
        '안마의자',
        '식기세척기',
        '공기청정기',
      ],
      logoFileName: 'sk-logo.png',
    }
    
  } // constructor
} // class sk

// 웅진코웨이 ===================================================
class coway extends main {
  constructor() {
    super();
    this.companyId = 'coway'; // 내부적으로 사용되는 키
    this.meta.title = '코웨이';
    this.meta.description = '코웨이 베스트 상품';
    this.config.mainColor= '#0099ff';
    this.config.secondColor= '#0073BF'; // 그라데이션에 사용
    this.config.mainTextColor= 'blue-text';
    // this.config.toctoc='https://partner.talk.naver.com/'; 네이버 톡톡 사용하지않음으로 비활성화
    this.db.categoryId = '코웨이'; // 관리자 페이지에 등록한 카테고리
    this.gnb = {
      on: true,
      menu: [
        '정수기',
        '공기청정기',
        '비데',
        '의류청정기',
        '매트리스',
        '주방가전',
      ],
      logoFileName: 'coway-logo.png',
    }
    
  } // constructor
} // class coway

// LG 퓨리케어 ===================================================
class lg extends main {
  constructor() {
    super();
    this.companyId = 'lg'; // 내부적으로 사용되는 키
    this.meta.title = 'LG 퓨리케어';
    this.meta.description = 'LG 베스트 상품';
    this.config.mainColor= '#ad1947';
    this.config.secondColor= '#6E0F2D'; // 그라데이션에 사용
    this.config.mainTextColor= 'pink-text';
    this.config.showLogo = false; // 상단에 로고를 보이지 않게 할때는 false로 한다.
    this.db.categoryId = 'LG'; // 관리자 페이지에 등록한 카테고리
    this.gnb = {
      on: true,
      menu: [
        '정수기',
        '공기청정기',
        '건조기',
        '스타일러',
        '전기레인지',
        '식기세척기',
        '안마의자',
        '홈브루',
      ],
      logoFileName: 'lg-logo.png',
    }
  } // constructor
} // class lg

// 쿠쿠 ===================================================
class cuckoo extends main {
  constructor() {
    super();
    this.companyId = 'cuckoo'; // 내부적으로 사용되는 키
    this.meta.title = '쿠쿠';
    this.meta.description = '쿠쿠 베스트 상품';
    this.config.mainColor= '#660000';
    this.config.secondColor= '#B30000'; // 그라데이션에 사용
    this.config.mainTextColor= 'brown-text';
    this.db.categoryId = '쿠쿠'; // 관리자 페이지에 등록한 카테고리
    this.gnb = {
      on: true,
      menu: [
        '정수기',
        '공기청정기',
        '비데',
        '안마의자',
        '전기레인지',
        '침구 및 가구',
        '반려동물용품',
      ],
      logoFileName: 'cuckoo-logo.png',
    }
    
  } // constructor
} // class cuckoo

// 청호나이스 ===================================================
class nice extends main {
  constructor() {
    super();
    this.companyId = 'nice'; // 내부적으로 사용되는 키
    this.meta.title = '청호나이스';
    this.meta.description = '베스트 상품';
    this.config.mainColor= '#19bcb9';
    this.config.secondColor= '#107D7B'; // 그라데이션에 사용
    this.config.mainTextColor= 'text-default';
    this.db.categoryId = '청호나이스'; // 관리자 페이지에 등록한 카테고리
    this.gnb = {
      on: true,
      menu: [
        '정수기',
        '공기청정기',
        '비데',
        '전기레인지',
        '세탁기',
        '건조기',
        '에어드레서',
      ],
      logoFileName: 'chungho-logo.png',
    }
    
  } // constructor
} // class nice

// cj 헬로렌탈 ===================================================
class hello extends main {
  constructor() {
    super();
    this.companyId = 'hello'; // 내부적으로 사용되는 키
    this.meta.title = 'CJ 헬로';
    this.meta.description = '베스트 상품';
    this.config.mainColor= '#7e159f';
    this.config.secondColor= '#4B0C5E'; // 그라데이션에 사용
    this.config.mainTextColor= 'purple-text';
    this.db.categoryId = 'CJ'; // 관리자 페이지에 등록한 카테고리
    this.gnb = {
      on: true,
      menu: [
        '가구',
        '생활가전',
        'PC 및 디지털',
        '엔터 및 유아',
        '주방가전',
        '뷰티 및 건강',
      ],
      logoFileName: 'cj-hello-logo.png',
    }
    
  } // constructor
} // class hello

// 현대 렌탈 ===================================================
class hycare extends main {
  constructor() {
    super();
    this.companyId = 'hycare'; // 내부적으로 사용되는 키
    this.meta.title = '현대케어';
    this.meta.description = '베스트 상품';
    this.config.mainColor= '#00917b';
    this.config.secondColor= '#005245'; // 그라데이션에 사용
    this.config.mainTextColor= 'text-default';
    this.db.categoryId = '현대'; // 관리자 페이지에 등록한 카테고리
    this.gnb = {
      on: true,
      menu: [
        '가구',
        '생활가전',
        'PC 및 디지털',
        '엔터 및 유아',
        '주방가전',
        '뷰티 및 건강',
      ],
      logoFileName: 'hy-care-logo.png',
    }
    
  } // constructor
} // class hello


// 웰릭스렌탈 ===================================================
class welrix extends main {
  constructor() {
    super();
    this.companyId = 'welrix'; // 내부적으로 사용되는 키
    this.meta.title = '웰릭스 렌탈';
    this.meta.description = '베스트 상품';
    this.config.mainColor= '#f5821f';
    this.config.secondColor= '#B56118'; // 그라데이션에 사용
    this.config.mainTextColor= 'red-text';
    this.db.categoryId = '웰릭스'; // 관리자 페이지에 등록한 카테고리
    this.gnb = {
      on: true,
      menu: [
        '환경가전',
        '주방가전',
        '생활가전',
        '뷰티 및 건강',
        '가구 및 침대',
      ],
      logoFileName: 'welrix-logo.png',
    }
    
  } // constructor
} // class welrix


// 루헨스 ===================================================
class ruhens extends main {
  constructor() {
    super();
    this.companyId = 'ruhens'; // 내부적으로 사용되는 키
    this.meta.title = '루헨스';
    this.meta.description = '베스트 상품';
    this.config.mainColor= '#228bfe';
    this.config.secondColor= '#1969BF'; // 그라데이션에 사용
    this.config.mainTextColor= 'red-text';
    this.db.categoryId = '루헨스'; // 관리자 페이지에 등록한 카테고리
    this.gnb = {
      on: true,
      menu: [
        '정수기',
        '공기청정기',
        '비데',
      ],
      logoFileName: 'ruhens-logo.png',
    }
    
  } // constructor
} // class welrix


// 교원웰스 ===================================================
class wells extends main {
  constructor() {
    super();
    this.companyId = 'wells'; // 내부적으로 사용되는 키
    this.meta.title = '교원웰스';
    this.meta.description = '베스트 상품';
    this.config.mainColor= '#00a6d0';
    this.config.secondColor= '#007491'; // 그라데이션에 사용
    this.config.mainTextColor= 'text-info';
    this.db.categoryId = '교원웰스'; // 관리자 페이지에 등록한 카테고리
    this.gnb = {
      on: true,
      menu: [
        '정수기',
        '공기청정기',
        '비데',
        '주방가전',
        '생활가전',
        '안마의자',
        '가구',
      ],
      logoFileName: 'wells-logo.png',
    }
    
  } // constructor
} // class welrix


// bs렌탈 ===================================================
class bs extends main {
  constructor() {
    super();
    this.companyId = 'bs'; // 내부적으로 사용되는 키
    this.meta.title = 'BS렌탈';
    this.meta.description = '베스트 상품';
    this.config.mainColor= '#24aae1';
    this.config.secondColor= '#1A7AA1'; // 그라데이션에 사용
    this.config.mainTextColor= 'text-info';
    this.db.categoryId = 'BS'; // 관리자 페이지에 등록한 카테고리
    this.gnb = {
      on: true,
      menu: [
        '최신가전',
        '헬스케어',
        '뷰티케어',
        '키친 및 가구',
        '기타제품',
      ],
      logoFileName: 'bs-logo.png',
    }
    
  } // constructor
} // class bs


// cs렌탈 ===================================================
class cs extends main {
  constructor() {
    super();
    this.companyId = 'cs'; // 내부적으로 사용되는 키
    this.meta.title = 'CS렌탈';
    this.meta.description = '베스트 상품';
    this.config.mainColor= '#396695';
    this.config.secondColor= '#203954'; // 그라데이션에 사용
    this.config.mainTextColor= 'text-info';
    this.db.categoryId = 'CS'; // 관리자 페이지에 등록한 카테고리
    this.gnb = {
      on: true,
      menu: [
        '생활가전',
        '계절가전',
        '환경가전',
        '주방가전',
        '헬스케어',
        '뷰티케어',
        '의료기기',
        '사업자전용',
      ],
      logoFileName: 'cs-logo.png',
    }
    
  } // constructor
} // class cs


// real렌탈 ===================================================
class real extends main {
  constructor() {
    super();
    this.companyId = 'real'; // 내부적으로 사용되는 키
    this.meta.title = 'Real렌탈';
    this.meta.description = '베스트 상품';
    this.config.mainColor= '#0f75bc';
    this.config.secondColor= '#0A4D7D'; // 그라데이션에 사용
    this.config.mainTextColor= 'text-info';
    this.db.categoryId = 'Real'; // 관리자 페이지에 등록한 카테고리
    this.gnb = {
      on: true,
      menu: [
        '일반가전',
        '생활가전',
        '주방가전',
        '계절가전',
        '모션베드',
        
      ],
      logoFileName: 'real-logo.png',
    }
    
  } // constructor
} // class real


// smart렌탈 ===================================================
class smart extends main {
  constructor() {
    super();
    this.companyId = 'smart'; // 내부적으로 사용되는 키
    this.meta.title = 'smart렌탈';
    this.meta.description = '베스트 상품';
    this.config.mainColor= '#005478';
    this.config.secondColor= '#002738'; // 그라데이션에 사용
    this.config.mainTextColor= 'blue-text';
    this.db.categoryId = '스마트'; // 관리자 페이지에 등록한 카테고리
    this.gnb = {
      on: true,
      menu: [
        '일반가전',
        '생활가전',
        '계절가전',
        '주방가전',
        '가구',       
      ],
      logoFileName: 'smart-logo.png',
    }
    
  } // constructor
} // class smart

module.exports = {main, sk, coway, lg, cuckoo, nice, hello, welrix, hycare, ruhens, wells, bs, cs, real, smart,};