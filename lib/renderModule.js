const request = require('request');

// 상품 리스트 가져오기
const getGoodList = (info, params) => {
  // console.log(params);
  return new Promise((resolve, reject) => {
    let options = {
      uri: info.apiDomain+'/api/list',
      method: 'POST',
      form : {
        show: true,
        format: true,
        category1: info.db.categoryId
      }
    }
    if (params.category != 'all') {
      options.form.category2 = params.category;
    }
    if (params.category2) {
      options.form.category3 = params.category2;
    }
    // console.log(options);
    request.post(options, (err, response, body) => {
      try {
        resolve(JSON.parse(body));
      }
      catch(e) {
        reject(err);
      }
    });  
  });
};

// 상품 상세 정보 가져오기
const getDetail = (info, params) => {
  return new Promise((resolve, reject) => {
    let options = {
      uri: info.apiDomain+'/api/detail',
      method: 'POST',
      form : {
        id: params.gid,
        format: true,
      }
    }
    request.post(options, (err, response, body) => {
      try {
        resolve(JSON.parse(body));
      }
      catch(e) {
        reject(err);
      }
    });  
  });
};

// 하위 카테고리 가져오기
const getCategory = (info, params) => {
  return new Promise((resolve, reject) => {
    let options = {
      uri: info.apiDomain+'/api/cate',
      method: 'POST',
      form : {
        categoryDepth: 3,
        parentCategory: params.category,
        parentCategory2: info.db.categoryId
      }
    }
    request.post(options, (err, response, body) => {
      try {
        resolve(JSON.parse(body));
      }
      catch(e) {
        reject(err);
      }
    });  
  });
};

// 배너 정보 가져오기
const getBanner = (info) => {
  return new Promise((resolve, reject) => {
    let options = {
      uri: info.apiDomain+'/api/banner/'+info.companyId,
      method: 'POST',
    }
    request.post(options, (err, response, body) => {
      try {
        resolve(JSON.parse(body));
      }
      catch(e) {
        reject(err);
      }
    });  
  });
};

// 이벤트 정보 가져오기
const getEvent = (info) => {
  return new Promise((resolve, reject) => {
    let options = {
      uri: info.apiDomain+'/api/event/'+info.companyId,
      method: 'POST',
    }
    request.post(options, (err, response, body) => {
      try {
        resolve(JSON.parse(body));
      }
      catch(e) {
        reject(err);
      }
    });  
  });
};

module.exports = {
  getGoodList,
  getDetail,
  getCategory,
  getBanner,
  getEvent,
};